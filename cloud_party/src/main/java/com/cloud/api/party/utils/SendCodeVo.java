package com.cloud.api.party.utils;

import lombok.Data;

@Data
public class SendCodeVo {
private String phone;
private String code;
}
