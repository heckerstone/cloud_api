package com.cloud.api.production.conf;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


//@Configuration
public class DefaultFeignLog {
    @Bean
    public Logger.Level level() {
        return Logger.Level.BASIC; // 日志级别为BASIC
    }
}
