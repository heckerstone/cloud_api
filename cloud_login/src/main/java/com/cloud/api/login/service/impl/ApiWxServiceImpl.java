package com.cloud.api.login.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.api.common.http.HttpUtil;
import com.cloud.api.common.http.URLUtil;
import com.cloud.api.common.result.R;
import com.cloud.api.common.utils.PageUtils;
import com.cloud.api.common.utils.Query;
import com.cloud.api.login.dao.ApiWxDao;
import com.cloud.api.login.entity.ApiWxEntity;
import com.cloud.api.login.service.ApiWxService;
import com.cloud.api.login.vo.WxEntityVo;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("apiWxService")
public class ApiWxServiceImpl extends ServiceImpl<ApiWxDao, ApiWxEntity> implements ApiWxService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ApiWxEntity> page = this.page(
                new Query<ApiWxEntity>().getPage(params),
                new QueryWrapper<ApiWxEntity>()

        );
        return new PageUtils(page);

    }

    /**
     * 动态获取各个小程序授权
     * @param code
     * @param type
     * @return
     */
    @Override
    public R queryApiWx(String code, String type) {

        ApiWxEntity one = this.baseMapper.selectOne(new QueryWrapper<ApiWxEntity>().eq("type", type));

        if (one == null) {
            return R.error("该小程序不存在！");
        }

        String data = (String) HttpUtil.HttpGet(one.getWxUrl().
                concat("appid=").concat(one.getAppId())
                .concat("&secret=").concat(one.getSecret())
                .concat("&js_code=").concat(code)
                .concat("&grant_type=").concat(one.getGrantType()), String.class);

        WxEntityVo wxEntityVo = JSONObject.parseObject(data, WxEntityVo.class);
        System.out.println(wxEntityVo);
        return R.ok(wxEntityVo);
    }


}