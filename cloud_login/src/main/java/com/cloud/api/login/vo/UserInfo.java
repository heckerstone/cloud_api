package com.cloud.api.login.vo;

import lombok.Data;

import java.util.List;

/**
 *
 * 公众号授权
 */
@Data
public class UserInfo {
    private String openid;

    private String nickname;

    private int sex;

    private String language;

    private String city;

    private String province;

    private String country;

    private String headimgurl;

    private List<String> privilege;


}
