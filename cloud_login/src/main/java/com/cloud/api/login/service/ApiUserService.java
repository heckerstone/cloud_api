package com.cloud.api.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.api.common.result.R;
import com.cloud.api.common.utils.PageUtils;
import com.cloud.api.login.entity.ApiUserEntity;

import java.util.Map;

/**
 * 
 *
 * @author å°å
 * @email 142277147@qq.com
 * @date 2022-11-17 22:54:39
 */
public interface ApiUserService extends IService<ApiUserEntity> {

    PageUtils queryPage(Map<String, Object> params);


    R getList();

    ApiUserEntity getOpenId(String openId);
}

