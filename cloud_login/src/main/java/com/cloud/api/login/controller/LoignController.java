package com.cloud.api.login.controller;

import com.alibaba.fastjson.JSONObject;
import com.cloud.api.common.http.HttpUtil;
import com.cloud.api.common.log.CloudApiAnnotation;
import com.cloud.api.common.result.R;
import com.cloud.api.login.service.ApiWxService;

import com.cloud.api.login.vo.TokenVo;
import com.cloud.api.login.vo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login/test")
public class LoignController {

    @Autowired
    private ApiWxService apiWxService;


    @CloudApiAnnotation(operMoudle = "登录接口", operMethod = "login", operDes = "测试")
    @GetMapping("/login")
    public R login(@RequestParam("id") String id) {
        return R.ok(id);

    }


    /**
     * 微信小程序code换取Token
     * QQ小程序code换取Token
     *
     * @param code
     * @param type
     * @return
     */
    @GetMapping("/loginCode")
    public R wxlogin(@RequestParam("code") String code, @RequestParam("type") String type) {
        return apiWxService.queryApiWx(code, type);

    }

    /**
     * 公众号H5授权登录
     * @param code
     * @return
     */
    @GetMapping("/loginh")
    public void wxH5(@RequestParam("code") String code) {
        System.out.println(code);
        String token = (String) HttpUtil.HttpGet("https://api.weixin.qq.com/sns/oauth2/access_token?appid=wxe65d71e3d0084dcf&secret=a578c3f65e1646f69a1fdf1480a48c1&code=" + code + "&grant_type=authorization_code", String.class);

        TokenVo tokenVo = JSONObject.parseObject(token, TokenVo.class);

        String user = (String) HttpUtil.HttpGet("https://api.weixin.qq.com/sns/userinfo?access_token="+tokenVo.getAccess_token()+"&openid="+tokenVo.getOpenid()+"&lang=zh_CN", String.class);
        System.out.println(user);
        UserInfo userInfo = JSONObject.parseObject(user, UserInfo.class);

        System.out.println(userInfo);

    }

}
