package com.cloud.api.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.api.common.result.R;
import com.cloud.api.common.utils.PageUtils;
import com.cloud.api.login.entity.ApiWxEntity;

import java.util.Map;

/**
 * 
 *
 * @author å°å
 * @email 142277147@qq.com
 * @date 2022-11-08 22:39:39
 */
public interface ApiWxService extends IService<ApiWxEntity> {

    PageUtils queryPage(Map<String, Object> params);


    R queryApiWx(String code,String type);
}

