package com.cloud.api.login.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author å°å
 * @email 142277147@qq.com
 * @date 2022-11-08 22:39:39
 */
@Data
@TableName("api_wx")
public class ApiWxEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 
	 */
	private String appId;
	/**
	 * 
	 */
	private String secret;
	/**
	 * 
	 */
	private String jsCode;
	/**
	 * 
	 */
	private String grantType;
	/**
	 * 
	 */
	private String wxUrl;
	/**
	 * 小程序名称
	 */
	private String name;
	/**
	 * 展商xcx_0,证票xcx_1,工作证xcx_2，观众正xcx_3
	 */
	private String type;
	/**
	 * 状态
	 */
	private String status;
	/**
	 * 
	 */
	private Date createTime;

}
