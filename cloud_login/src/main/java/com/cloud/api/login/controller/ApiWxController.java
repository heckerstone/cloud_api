package com.cloud.api.login.controller;

import java.util.Arrays;
import java.util.Map;

import com.cloud.api.common.result.R;
import com.cloud.api.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.api.login.entity.ApiWxEntity;
import com.cloud.api.login.service.ApiWxService;



/**
 * 
 *
 * @author å°å
 * @email 142277147@qq.com
 * @date 2022-11-08 22:39:39
 */
@RestController
@RequestMapping("login/apiwx")
public class ApiWxController {
    @Autowired
    private ApiWxService apiWxService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = apiWxService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
		ApiWxEntity apiWx = apiWxService.getById(id);
        System.out.println(
                apiWx
        );
        return R.ok().put("apiWx", apiWx);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ApiWxEntity apiWx){
		apiWxService.save(apiWx);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ApiWxEntity apiWx){
		apiWxService.updateById(apiWx);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
		apiWxService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
