package com.cloud.api.data.controller;

import com.cloud.api.common.log.CloudApiAnnotation;
import com.cloud.api.data.feign.DataProductionFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("data/dev")
public class DataController {


    @Autowired
    DataProductionFeignService dataProductionFeignService;


    @CloudApiAnnotation(operMoudle = "测试接口",operMethod="getName", operDes = "测试")
    @GetMapping("ok")
    public String getName(@RequestParam("id") String id) {

//        String name = null;
//        System.out.println(name.length());
//        int i = 1 / 0;
        String production = dataProductionFeignService.getProduction(id);
        System.out.println("我是调用者：" + production);
        return production;
    }

}