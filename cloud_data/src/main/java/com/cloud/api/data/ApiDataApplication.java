package com.cloud.api.data;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.TimeUnit;


/**
 * @Author 小坏
 * @Date 2022/10/29 14:44
 * @Version 1.0
 * @program: 数据处理
 */
@EnableFeignClients
@SpringBootApplication(scanBasePackages = "com.cloud.api")
public class ApiDataApplication {


    public static void main(String[] args) throws InterruptedException {
        System.setProperty("nacos.logging.default.config.enabled", "false");
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ApiDataApplication.class, args);


        while (true) {
            //当动态配置刷新时，会更新到 Enviroment中，因此这里每隔一秒中从Enviroment中获取配置
            String age = applicationContext.getEnvironment().getProperty("user.name");

            System.err.println("user age :" + age);
            TimeUnit.SECONDS.sleep(1);
        }
    }
}


//    @Bean
//    public Request.Options options(){
//        // Request.Options(int connectTimeoutMillis, int readTimeoutMillis)方法已经过时
//        return new Request.Options(90000L,90000L);
//    }


