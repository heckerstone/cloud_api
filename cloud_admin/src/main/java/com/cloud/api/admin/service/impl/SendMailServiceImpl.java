package com.cloud.api.admin.service.impl;

import com.cloud.api.admin.service.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class SendMailServiceImpl implements SendMailService {
    @Autowired
    private JavaMailSender javaMailSender;

    //发送人
    private String from = "1477227147@qq.com";
    //接收人
    private String to = "1477227147@qq.com";
    //标题
    private String subject = "cloud_api服务报警";


    @Override
    public void sendMail(String msg) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from + "(小甜甜)");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(msg);
        javaMailSender.send(message);
    }
}
