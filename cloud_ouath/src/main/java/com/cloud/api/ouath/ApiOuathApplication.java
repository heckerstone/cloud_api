package com.cloud.api.ouath;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author 小坏
 * @Date 2022/10/29 14:44
 * @Version 1.0
 * @program: 接口鉴权
 */
@EnableFeignClients
@SpringBootApplication(scanBasePackages = "com.cloud.api")
public class ApiOuathApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiOuathApplication.class, args);
    }
}