# uexpo-api

#### 介绍
Spring Cloud Alibaba 致力于提供分布式应用服务开发的一站式解决方案。此项目包含开发分布式应用服务的必需组件，方便开发者通过 Spring Cloud 编程模型轻松使用这些组件来开发分布式应用服务。该项目使用如下组件搭建起来的微服务架构和各种授权登录模块，以及支付封装

#### 开发作者：小坏
#### 联系方式：QQ群 530383698
#### 搭建教程： [(JAVA版)SpringCloud框架开发教程(SpringCloudAlibaba微服务分布式架构丨kubesphere云原生)](https://www.bilibili.com/video/BV1c84y167ud/?share_source=copy_web&vd_source=e75cf83a888009fed85a1094910b7dbb)
#### 软件架构
| SpringCloud-Alibaba                   | 微服务技术架构点                 |
|---------------------------------------|--------------------------|
| Spring Cloud Alibaba Nacos            | 注册中心                     |
| Spring Cloud Alibaba Config           | 配置中心                     |
| Spring Cloud Alibaba Sentinel         | 限流、熔断、降级、自定义配置           |
| Spring Cloud Alibaba Cloud OSS        | 微服务自带OSS存储、解决繁琐的配置       |
| Spring Cloud Alibaba Cloud SchedulerX | 微服务自带的定时任务               |
| Spring Cloud Alibaba Cloud Seata      | 分布式事务解决方案                |
| SpringBoot2.0                         | 快速开发框架、微服务的依赖            |
| SpringBootAdminUI                     | 实时监控每个服务的状态              |
| Spring Cache                          | 缓存                       |
| SpringSession                         | 跨子域Session同步方案           |
| Mybatis-Plus                          | 开发必备的                    |
| MySQL                                 | 5.7                      |
| MonGoDB                               | 分布式文件存储的数据库、高性能数据存储      |
| Redis                                 | 分布式锁、缓存存储、内存穿透、雪崩   |
| Redission                             | 解决多个服务session共享问题、分布式信号量 |
| ES                                    | 最强的的搜索引擎、查询速度快、RESUT风格   |
| XXL-JOB                               | 分布式定时任务、解决多个实例产生的幂等性的问题  |
| RabbitMQ                              | 实现最终一致性、类似小程序订单超时自动关单    |
| KafKa                                 | 俗称每秒QPS可达百万、处理数据中台       |
| MQTT                                  | 软硬件传输数据 （物联网必备）          |
| zipkin                                | 链路追踪、实时查看每个服务的链路、请求时间    |


| 异常推送| 钉钉推送  |
|---|---|
|  DD | git提交通知  |
|  程序异常推送 |  异常报警通知 |


#### 安装教程

1.  启动 NACOS 注册中心
2.  启动 Sentinel 监控中心
3.  启动 Zinkin 链路追踪


#### 工程服务说明



